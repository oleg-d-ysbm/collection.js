// collection.js

module.exports = {
    create: function (index, $function) {
        var newArr = [];
        for (var i = 0; i < index; i++) {
            newArr[newArr.length] = $function(i);
        }

        return newArr;
    },
    map: function (arr, $function) {
        var newArr = [];
        for (var i = 0, l = arr.length; i < l; i++) {
            newArr[i] = $function(arr[i], i, arr);
        }

        return newArr;
    },
    every: function (arr, isBig) {
        var every = true;
        for (var i = 0; i < arr.length; i++) {
            if (!isBig(arr[i])) {
                every = false;
            }
        }

        return every;
    },
    none: function (arr, isCool) {
        var none = true;
        for (var i = 0; i < arr.length; i++) {
            if (isCool(arr[i])) {
                none = false;
            }
        }

        return none;
    },
    uniq: function (arr, $function) {
        var newArr = [];
        for (var i = 0, l = arr.length; i < l; i++) {
            newArr[newArr.length] = $function(arr[i], i, arr);
        }

        var uniqArr = [];
        for (var newArrKey = 0; newArrKey < newArr.length; newArrKey++) {
            var found = false;
            for (var uniqArrKey = 0; uniqArrKey < uniqArr.length; uniqArrKey++) {
                if (newArr[newArrKey] === uniqArr[uniqArrKey]) {
                    found = true;
                }
            }
            if (!found) {
                uniqArr[uniqArr.length] = newArr[newArrKey];
            }
        }

        return uniqArr;
    },
    add: function () {
        var arrayFunc = function(array) {
            var newArr = [];
            for ( var i = 0; i < array.length; i++ ) {
                // Check Array or not
                if (array[i] && typeof array[i] === 'object' && typeof array[i].length === 'number') {
                    var list = arrayFunc(array[i]);
                    for ( var list_key = 0; list_key < list.length; list_key++ ) {
                        newArr[newArr.length] = list[list_key];
                    }
                } else {
                    newArr[newArr.length] = array[i];
                }
            }

            return newArr;
        };

        return arrayFunc(arguments);
    }
};
